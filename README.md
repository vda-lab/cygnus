# Cygnus

Cygnus is a set of HTML elements that give you the opportunity to create visualizations on the web based on The Grammar of Graphics. The elements have been implemented on top of D3.js through Polymer and Web Components.

## Example

![alternate text](https://bytebucket.org/vda-lab/cygnus/raw/master/figures/f1.png)

```
<c-csv data={{data}} file="iris.csv" x="sepalLength" y="sepalWidth" group="species"></c-csv>
<c-layout anchor="{{anchor}}"></c-layout>
<c-points anchor="{{anchor}}" data={{data}}></c-points>
<c-axis anchor="{{anchor}}" data={{data}}></c-axis>
<c-legend anchor="{{anchor}}" data={{data}}></c-legend>
<c-lab anchor="{{anchor}}" x ="Sepal Length" y="Sepal Width"></c-labs>
```

## What elements are available?

* Data parser: c-csv, c-tsv
* SVG container : c-layout
* Guides: c-axis, c-legend, c-lab
* Visual Elements: c-bars, c-lines, c-parcoords, c-path, c-points, c-polygon, c-text